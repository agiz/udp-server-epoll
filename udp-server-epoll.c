#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <errno.h>
#include <sys/socket.h>
#include <linux/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <linux/netfilter.h>		/* for NF_ACCEPT */

#define MAXEVENTS 64
#define MAXBUF 512
#define SERVER_IP "192.168.1.62"
#define SERVER_PORT 9930

struct nfq_q_handle *qh_tmp = NULL;
int id_tmp = -1;
int s2;
struct sockaddr_in si_other;
int slen = sizeof(si_other);

struct nfq_iphdr
{
	/*
#if defined(__LITTLE_ENDIAN_BITFIELD)
uint8_t  ihl:4,
version:4;
#elif defined (__BIG_ENDIAN_BITFIELD)
*/
	uint8_t   version:4,
			  ihl:4;
	/*
#endif
*/
	uint8_t  tos;
	uint16_t  tot_len;
	uint16_t  id;
	uint16_t  frag_off;
	uint8_t  ttl;
	uint8_t  protocol;
	uint16_t check;
	uint32_t  saddr;
	uint32_t  daddr;
};

struct nfq_tcphdr
{
	uint16_t  source;
	uint16_t  dest;
	uint32_t  seq;
	uint32_t  ack_seq;
	/*
#if defined(__LITTLE_ENDIAN_BITFIELD)
uint16_t res1:4,
doff:4,
fin:1,
syn:1,
rst:1,
psh:1,
ack:1,
urg:1,
ece:1,
cwr:1;
#elif defined(__BIG_ENDIAN_BITFIELD)
*/
	uint16_t doff:4,
			  res1:4,
			  cwr:1,
			  ece:1,
			  urg:1,
			  ack:1,
			  psh:1,
			  rst:1,
			  syn:1,
			  fin:1;
	/*
#endif
*/
	uint16_t  window;
	uint16_t check;
	uint16_t  urg_ptr;
};

struct nfq_udphdr
{
	uint16_t source;
	uint16_t dest;
	uint16_t len;
	uint16_t check;
};

extern struct nfq_iphdr* nfq_get_iphdr(struct nfq_data *nfad) {
    char *data;
    if (nfq_get_payload(nfad, &data) == -1)
        return NULL;
    return (struct nfq_iphdr*) data;
}

extern uint32_t nfq_get_ip_saddr(struct nfq_iphdr *hdr)
{
    return ntohl(hdr->saddr);
}

extern uint32_t nfq_get_ip_daddr(struct nfq_iphdr *hdr)
{
    return ntohl(hdr->daddr);
}

static int
manage_packet (struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
		struct nfq_data *nfa, void *data2)
{
	//char *payload;
	int id = -1;
	struct nfqnl_msg_packet_hdr *packetHeader;
	struct nfq_iphdr* ip_header;

	if ((packetHeader = nfq_get_msg_packet_hdr(nfa)) != NULL) {
		id = ntohl(packetHeader->packet_id);
	}

	//nfq_get_payload(nfa, &payload);
	ip_header = nfq_get_iphdr(nfa);

	qh_tmp = qh;
	id_tmp = id;
	printf("accept %d\n", id);
	char buf[512];
	//sprintf(buf, "PACKET %d - PAYLOAD: %s\n", id, payload);
	//sprintf(buf, "PACKET %d - PAYLOAD: \n", id);
	sprintf(buf, "source: %d\ndest: %d\n", nfq_get_ip_saddr(ip_header), nfq_get_ip_daddr(ip_header));
	if (sendto(s2, buf, MAXBUF, 0, (struct sockaddr *)&si_other, slen) == -1) {
		printf("NO?\n");
		return -1;
	}
	//nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);
	return id;
}

static int
make_socket_non_blocking (int sfd)
{
	int flags, s;

	flags = fcntl (sfd, F_GETFL, 0);
	if (flags == -1) {
		perror ("fcntl");
		return -1;
	}

	flags |= O_NONBLOCK;
	s = fcntl (sfd, F_SETFL, flags);
	if (s == -1) {
		perror ("fcntl");
		return -1;
	}

	return 0;
}

static int
create_and_bind (char *port)
{
	int ld;
	struct sockaddr_in skaddr;
	int length;

	/* create a socket
		 IP protocol family (PF_INET)
		 UDP protocol (SOCK_DGRAM)
	*/

	if ((ld = socket(PF_INET, SOCK_DGRAM, 0)) < 0) {
		printf("Problem creating socket\n");
		exit(1);
	}

	/* establish our address
		 address family is AF_INET
		 our IP address is INADDR_ANY (any of our IP addresses)
		 the port number is assigned by the kernel
	*/

	skaddr.sin_family = AF_INET;
	skaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	skaddr.sin_port = htons(9999);

	if (bind(ld, (struct sockaddr *) &skaddr, sizeof(skaddr)) < 0) {
		printf("Problem binding\n");
		exit(0);
	}

	/* find out what port we were assigned and print it out */

	length = sizeof(skaddr);
	if (getsockname(ld, (struct sockaddr *) &skaddr, &length) < 0) {
		printf("Error getsockname\n");
		exit(1);
	}

	/* port number's are network byte order, we have to convert to
		 host byte order before printing !
	*/
	printf("The server UDP port number is %d\n",ntohs(skaddr.sin_port));

	return ld;
}

int
main (int argc, char *argv[])
{
	int sfd, s;
	int efd;
	struct epoll_event event, event2;
	struct epoll_event *events;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s [port]\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	// Create connection to server.
	//char buf[MAXBUF];
	if ((s2 = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		printf("8\n");
		return -1;
	}

	memset((char *) &si_other, 0, sizeof(si_other));
	si_other.sin_family = AF_INET;
	si_other.sin_port = htons(SERVER_PORT);
	if (inet_aton(SERVER_IP, &si_other.sin_addr) == 0) {
		fprintf(stderr, "inet_aton() failed\n");
		return -1;
	}

	// Listen for incoming connections.
	sfd = create_and_bind(argv[1]);
	if (sfd == -1) {
		abort();
	}

	//s = make_socket_non_blocking (sfd);
	//if (s == -1) {
	//	abort ();
	//}

	efd = epoll_create1(0);
	if (efd == -1) {
		perror("epoll_create");
		abort();
	}

	event.data.fd = sfd;
	event.events = EPOLLIN | EPOLLET;
	s = epoll_ctl(efd, EPOLL_CTL_ADD, sfd, &event);
	if (s == -1) {
		perror ("epoll_ctl");
		abort ();
	}

	// Create second event!
	struct nfq_handle *handle;
	struct nfq_q_handle *queue;
	struct nfnl_handle *netlink_handle;
	int nfqueue_fd;

	// NF_QUEUE initializing
	handle = nfq_open();
	if (!handle) {
		perror("Error: during nfq_open()");
		goto end;
	}

	if (nfq_unbind_pf(handle, AF_INET) < 0) {
		perror("Error: during nfq_unbind_pf()");
		goto end;
	}

	if (nfq_bind_pf(handle, AF_INET) < 0) {
		perror("Error: during nfq_bind_pf()");
		goto end;
	}

	queue = nfq_create_queue(handle, 0, &manage_packet, NULL);
	if (!queue) {
		perror("Error: during nfq_create_queue()");
		goto end;
	}

	//NFQNL_COPY_NONE - do not copy any data
	//NFQNL_COPY_META - copy only packet metadata
	//NFQNL_COPY_PACKET - copy entire packet
	if (nfq_set_mode(queue, NFQNL_COPY_PACKET, 0xffff) < 0)
	{
		perror("Error: can't set packet_copy mode");
		goto end;
	}

	netlink_handle = nfq_nfnlh(handle);
	nfqueue_fd = nfnl_fd(netlink_handle);
	// End of NF_QUEUE initializing

	//s = make_socket_non_blocking (nfqueue_fd);
	//if (s == -1)
	//	abort ();

	event2.data.fd = nfqueue_fd;
	event2.events = EPOLLIN | EPOLLET;
	s = epoll_ctl(efd, EPOLL_CTL_ADD, nfqueue_fd, &event2);
	if (s == -1) {
		perror("epoll_ctl");
		abort();
	}

	/* Buffer where events are returned */
	events = calloc (MAXEVENTS, sizeof event);

	printf("A1\n");

	/* The event loop */
	while (1)
	{
		int n, i;
		n = epoll_wait(efd, events, MAXEVENTS, -1);
		for (i = 0; i < n; i++) {
			printf("EV: %d\n", i);
			if ((events[i].events & EPOLLERR) ||
				(events[i].events & EPOLLHUP) ||
				(!(events[i].events & EPOLLIN)))
			{
				printf("6\n");
				/* An error has occured on this fd, or the socket is not
					 ready for reading (why were we notified then?) */
				fprintf(stderr, "epoll error\n");
				close(events[i].data.fd);
				continue;
			}
			else if (sfd == events[i].data.fd)
			{
				printf("1\n");
				int len, n1;
				char bufin[MAXBUF];
				struct sockaddr_in remote;
				// need to know how big address struct is, len must be set before the call to recvfrom!!!
				len = sizeof(remote);

				//while (1)
				{
					n1 = recvfrom(sfd, bufin, MAXBUF, 0, (struct sockaddr *)&remote,&len);

					/* print out the address of the sender */
					printf("Got a datagram from %s port %d\n",
								 inet_ntoa(remote.sin_addr), ntohs(remote.sin_port));

					if (n1 < 0) {
						perror("Error receiving data");
					}
					else {
						printf("GOT %d BYTES EVENT: %d\n", n1, i);
						nfq_set_verdict(qh_tmp, id_tmp, NF_ACCEPT, 0, NULL);
						/* Got something, just send it back */
						sendto(sfd, bufin, n1, 0, (struct sockaddr *)&remote, len);
					}
				}
				continue;
			}
			else if (nfqueue_fd == events[i].data.fd)
			{
				printf("2\n");
				//while (1)
				{
					char bufx[4096] __attribute__ ((aligned));
					int received;
					received = recv(nfqueue_fd, bufx, sizeof(bufx), 0);
					printf("EVENT: %d RECEIVED: %d\n", i, received);
					if (received == -1) {
						return;
					}
					// Call the handle
					nfq_handle_packet(handle, bufx, received);
				}
				continue;
			}
			else
			{
				/* We have data on the fd waiting to be read. Read and
					 display it. We must read whatever data is available
					 completely, as we are running in edge-triggered mode
					 and won't get a notification again for the same
					 data. */
				int done = 0;

				while (1)
				{
					ssize_t count;
					char buf[512];

					count = read(events[i].data.fd, buf, sizeof buf);
					if (count == -1) {
						/* If errno == EAGAIN, that means we have read all
							 data. So go back to the main loop. */
						if (errno != EAGAIN) {
							perror ("read");
							done = 1;
						}
						break;
					}
					else if (count == 0) {
						/* End of file. The remote has closed the
							 connection. */
						done = 1;
						break;
					}

					/* Write the buffer to standard output */
					s = write(1, buf, count);
					if (s == -1) {
						perror("write");
						abort();
					}
				}

				if (done)
				{
					printf("Closed connection on descriptor %d\n",
									events[i].data.fd);

					/* Closing the descriptor will make epoll remove it
						 from the set of descriptors which are monitored. */
					close(events[i].data.fd);
				}
			}
		}
	}

end:
	// Free the mallocs
	if (queue != NULL) {
		nfq_destroy_queue(queue);
	}
	if (handle != NULL) {
		nfq_close(handle);
	}

	free(events);
	close(sfd);
	close(s2);

	return EXIT_SUCCESS;
}

